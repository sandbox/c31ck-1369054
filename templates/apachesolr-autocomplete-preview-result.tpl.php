<?php
/**
 * Template for theming a single row of the autocomplete results.
 * Available variables:
 *   - $first:    TRUE/FALSE. Is this item the first item in a section. A 
 *                section is for example 'Collections'.
 *   - $result:   The result returned by our search action. Contains node data.
 *   - $settings: The setttings associated with this result. Contains the
 *                section, content_type, number of results,...               
 */
?>
<?php
  $link_content = '';
?>
<div class="autocomplete-item">
  <?php if ($variables['first']): ?>
    <?php $link_content .= '<span class="autocomplete-section">' . $variables['settings']['section'] . '</span>'; ?>
  <?php endif; ?>
  <?php
    $link_content .= '<span class="autocomplete-key">' . $variables['result']->label . '</span>';
  ?>
  <?php echo l($link_content, $variables['result']->url, array('html' => TRUE)); ?>
</div>