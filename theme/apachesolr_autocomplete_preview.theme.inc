<?php

/**
 * @file
 *   Theme functions for apachesolr_autocomplete_preview module.
 *   
 */

/**
 * Themes the search results for use in the autocomplete dropdown.
 * 
 * @param $results
 *   List of results, grouped per node type.
 * @param $settings 
 *   Settings
 */
function theme_apachesolr_autocomplete_preview_results($variables) {
  $output = array();
  // Theme our results
  foreach ($variables['results'] as $content_type => $results) {
    $i = 0;
    foreach ($results as $result) {
      $node = $result['node'];
      $output[] = theme('apachesolr_autocomplete_preview_result', array('result' => $node, 'settings' => $variables['settings'][$content_type], 'first' => ($i == 0)));
      $i++;
    }
  }
  // Add the number of results found.
  $output[] = theme('apachesolr_autocomplete_preview_number_of_results', 
                array('number_of_results' => $variables['number_of_results'],
                                                        'settings' => $variables['settings'],
                                                        'keys' => $variables['keys']
                     )
                );
  return $output;
}

/**
 * Theme the 'show all results' part of the autocomplete.
 * 
 * @param type $variables 
 *   - number_of_results: number of results
 *   - keys: keyword(s) that were entered in the search input.
 */
function theme_apachesolr_autocomplete_preview_number_of_results($variables) {
  $link_text = t('See all @number_of_results results', array('@number_of_results' => $variables['number_of_results']));
  $link = l($link_text, 'search/site/' . $variables['keys'], array('html' => TRUE));
  return '<div class="autocomplete-item autocomplete-item-count">' . $link . '</div>';
}
