<?php

/**
 * Implements hook_theme().
 */
function apachesolr_autocomplete_preview_theme() {
  return array(
    'apachesolr_autocomplete_preview_results' => array(
      'path' => drupal_get_path('module', 'apachesolr_autocomplete_preview') . '/theme',
      'file' => 'apachesolr_autocomplete_preview.theme.inc',
      'arguments' => array(
        'results' => array(),
        'settings' => array(),
        'number_of_results' => NULL,
        'keys' => '',  
      ),
    ),
    'apachesolr_autocomplete_preview_result' => array(
      'path' => drupal_get_path('module', 'apachesolr_autocomplete_preview') . '/templates',
      'template' => 'apachesolr-autocomplete-preview-result',  
      'arguments' => array(
        'result' => array(),
        'settings' => array(),
      ),
    ),
    'apachesolr_autocomplete_preview_number_of_results' => array(
      'path' => drupal_get_path('module', 'apachesolr_autocomplete_preview') . '/theme',
      'file' => 'apachesolr_autocomplete_preview.theme.inc',
      'arguments' => array(
        'number_of_results' => NULL,
        'keys' => '',
      ),
    ),
  );
}

/**
 * Implements hook_js_alter().
 * 
 * Get rid of the apachesolr_autocomplete.js that submits the search form
 * when a value is chosen in the dropdown.
 * 
 * @param javascript
 *    An array of all JavaScript being presented on the page.
 * 
 */
function apachesolr_autocomplete_preview_js_alter(&$javascript) {
  $js_id = drupal_get_path('module', 'apachesolr_autocomplete') . '/apachesolr_autocomplete.js';
  unset($javascript[$js_id]);
}

/**
 * Implements hook_init().
 * 
 * Add our own javascript to override apachesolr_autocomplete.js
 */
function apachesolr_autocomplete_preview_init() {
  // If using custom JS widget.
  if (apachesolr_autocomplete_variable_get_widget() == 'custom') {
    drupal_add_js(drupal_get_path('module', 'apachesolr_autocomplete_preview') . '/js/apachesolr_autocomplete_preview.js');
  }
  drupal_add_css(drupal_get_path('module', 'apachesolr_autocomplete_preview') . '/css/apachesolr_autocomplete_preview.css');
}

/**
 * Implements hook_menu_alter().
 */
function apachesolr_autocomplete_preview_menu_alter(&$items) {
  $items['apachesolr_autocomplete']['page callback'] = 'apachesolr_autocomplete_preview_callback';
}

/**
 * Callback for autocomplete functionality.
 * 
 * @param $keys
 *   The user-entered query.
 */
function apachesolr_autocomplete_preview_callback($keys = '') {
  if (apachesolr_autocomplete_variable_get_widget() == 'custom') {
    $keys = $_GET['query'];
  }
  // We'll only do autocomplete when $keys is at least 3 characters.
  if (strlen($keys) < 3) {
    return array();
  } 
  $suggestions = array();
  $suggestions = array_merge($suggestions, apachesolr_autocomplete_suggest_nodes($keys, 5));
  
  /*if (apachesolr_autocomplete_variable_get_suggest_keywords() || apachesolr_autocomplete_variable_get_suggest_spellcheck()) {
    $suggestions = array_merge($suggestions, apachesolr_autocomplete_suggest_additional_term($keys, 5));
  }*/
  
  $result = array();
  if (apachesolr_autocomplete_variable_get_widget() == 'custom') {
    // Place suggestions into new array for returning as JSON.
    foreach ($suggestions as $key => $display) {
      $result[] = array(
        "key" => $keys,
        "display" => $display
      );
    }
  }
  else {
    foreach ($suggestions as $key => $display) {
      $result[substr($key, 1)] = $display;
    }
  }
  drupal_json_output($result);
  exit();
}

/**
 * Helper function that suggests content.
 */
function apachesolr_autocomplete_suggest_nodes($keys, $suggestions_to_return = 5) {
  // Get the default settings.
  $settings = _apachesolr_autocomplete_preview_get_settings();
  // Allow other modules to override our settings.
  drupal_alter('apachesolr_autocomplete_preview_settings', $settings);
  
  /**
   * Split $keys into two:
   *  $first_part will contain all complete words (delimited by spaces). Can be empty.
   *  $last_part is the (assumed incomplete) last word. If this is empty, don't suggest.
   * Example:
   *  $keys = "learning dis" : $first_part = "learning", $last_part = "dis"
   */
  preg_match('/^(:?(.* |))([^ ]+)$/', $keys, $matches);
  $first_part = @$matches[2];
  // Make sure $last_part contains meaningful characters
  $last_part = preg_replace('/[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . ']+/u', '', @$matches[3]);
  if ($last_part == '') {
    return array();
  }
  
  // Ask Solr to return facets that begin with $last_part; these will be the suggestions.
  $params = apachesolr_autocomplete_basic_params($suggestions_to_return);
  $params['facet.prefix'] = $last_part;
  $suggestions = apachesolr_autocomplete_preview_suggest($first_part, $params, $keys, $settings, $suggestions_to_return);
  $suggestions = theme('apachesolr_autocomplete_preview_results', array('results' => $suggestions['results'],
                                                                        'settings' => $settings,
                                                                        'number_of_results' => $suggestions['number_of_results'],
                                                                        'keys' => $keys,  
                                                                       )
                 );
  return $suggestions;
}

/**
 * Provide node suggestions.
 * 
 * @param $keys
 *   The user entered query.
 * @param $params
 *   Query params.
 * @param $orig_keys
 *   Original keys
 * @param $settings
 *   Settings.
 * @param $suggestions_to_return
 *   Number of suggestions to return.
 * @return 
 */
function apachesolr_autocomplete_preview_suggest($keys, $params, $orig_keys, $settings, $suggestions_to_return = 5) {
  $mathes = array();
  $results = array();
  $keys = trim($keys);
  
  // We need the keys array to make sure we don't suggest words that are already
  // in the search terms.
  $keys_array = explode(' ', $keys);
  $keys_array = array_filter($keys_array);
  
  // Query Solr for $keys so that suggestions will always return results.
  $query = apachesolr_drupal_query($keys);

  // This hook allows modules to modify the query and params objects.
  drupal_alter('apachesolr_query', $query);
  if (!$query) {
    return array();
  }
  apachesolr_search_add_spellcheck_params($query);
  foreach ($params as $param => $paramValue) {
    $query->addParam($param, $paramValue);
  }
 
  apachesolr_search_add_boost_params($query);
  
  $response = $query->search($keys);
  
  foreach ($params['facet.field'] as $field) {
    foreach ($response->facet_counts->facet_fields->{$field} as $terms => $count) {

      $terms = preg_replace('/[_-]+/', ' ', $terms);

      foreach (explode(' ', $terms)  as $term) {
        if ($term = trim(preg_replace('/[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . ']+/u', '', $term))) {
          if (isset($matches[$term])) {
            $matches[$term] += $count;
          }
          else {
            $matches[$term] = $count;
          }
        }
      }
    }
  }
  
  if (sizeof($matches) > 0) {
    // Eliminate suggestions that are stopwords or are already in the query.
    $matches_clone = $matches;
    $stopwords = apachesolr_autocomplete_get_stopwords();
    foreach ($matches_clone as $term => $count) {
      if ((strlen($term) > 3) && !in_array($term, $stopwords) && !array_search($term, $keys_array)) {
        // Longer strings get higher ratings.
        #$matches_clone[$term] += strlen($term);
      }
      else {
        unset($matches_clone[$term]);
        unset($matches[$term]);
      }
    }

    // The $count in this array is actually a score. We want the highest ones first.
    arsort($matches_clone);

    // Shorten the array to the right ones.
    $matches_clone = array_slice($matches_clone, 0, $suggestions_to_return, TRUE);
    
    // Add current search as suggestion if results > 0
    if ($response->response->numFound > 0 && $keys != '') {
      // Add * to array element key to force into a string, else PHP will
      // renumber keys that look like numbers on the returned array.
      $suggestions[] = $keys;
    }

    // Build suggestions using returned facets
    foreach ($matches_clone as $match => $count) {
      if ($keys != $match) {
        $suggestion = trim($keys . ' ' . $match);
        
        if ($suggestion != '') {
          // Add * to array element key to force into a string, else PHP will
          // renumber keys that look like numbers on the returned array.
          $suggestions[] = $suggestion;
        }
      }
    }
  }
  $results = array();
  foreach ($settings as $index => $setting) {
    // No suggestions
    if($suggestions === NULL) {
      return array();
    }
    $keys = implode(' ', $suggestions);
    // No keys available.
    if ($keys === NULL) {
      return array();
    }
    $params = array();
    $params['q'][] = $keys;
    $query_suffix = isset($setting['content_type']) ? $setting['content_type'] : 'all';
    $result = apachesolr_search_run('apachesolr-autocomplete-preview-' . $query_suffix, $params, '$score');
    $results[$query_suffix] = $result;
  }
  // count the results
  $count = count(apachesolr_search_run('apachesolr_total', array('q' => array($keys))));
  return array('results' => $results, 'number_of_results' => $count, 'keys' => $keys);
}

/**
 * Provides the default settings: show 5 node titles, don't filter by node
 * type, don't show a section.
 * 
 * @return array
 *   Default settings. 
 */
function _apachesolr_autocomplete_preview_get_settings() {
  $default_settings = array(
    0 => array(
      'number_of_results' => 5,
      'section' => '',
    ),
  );
  return $default_settings;
}

/**
 * Implements hook_apachesolr_query_alter().
 * 
 * @param $query
 *   The apachesolr query about to be executed.
 */
function apachesolr_autocomplete_preview_apachesolr_query_alter(&$query) {
  if (strpos($query->getName(), 'apachesolr-autocomplete-preview') === 0) {
    $settings = _apachesolr_autocomplete_preview_get_query_params($query->getName());
    // Set the number of results.
    $params['rows'] = (int) isset($settings['number_of_results']) ? $settings['number_of_results'] : 5;
    // Set the content type filter
    if (isset($settings['content_type']) && ($settings['content_type'] != 'all')) {
      $params['fq'][] = 'bundle:' . $settings['content_type'];
    }
    // Search in title only.
    if (variable_get('apachesolr_autocomplete_preview_search_title_only', TRUE) === TRUE) {
      $query->replaceParam('qf', 'label');
    }
    $query->addParams($params);
  }
}

/**
 * Gets the query params based on the settings.
 *
 * @param $query_name
 *   Name of the query
 * 
 * @return
 *   An array of settings
 */
function _apachesolr_autocomplete_preview_get_query_params($query_name) {
  $content_type = str_replace('apachesolr-autocomplete-preview-', '', $query_name);
  $settings = _apachesolr_autocomplete_preview_get_settings();
  // Allow other modules to override our settings.
  drupal_alter('apachesolr_autocomplete_preview_settings', $settings);
  if (is_string($content_type) && strlen($content_type) > 0 
      && isset($settings[$content_type]) && is_array($settings[$content_type])) {
    return $settings[$content_type];
  } elseif ($content_type == 'all' && isset($settings[0]) && is_array($settings[0])) {
    return $settings[0];
  }
  return array();
}